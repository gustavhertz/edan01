import org.jacop.constraints.LinearInt;
import org.jacop.constraints.XeqY;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.IndomainMax;
import org.jacop.search.IndomainMin;
import org.jacop.search.Search;
import org.jacop.search.SelectChoicePoint;
import org.jacop.search.SimpleSelect;

public class Excercise1 {

	private Store store;

	public static void main(String[] args) {
		Excercise1 ex = new Excercise1();
		ex.calc();
	}

	@SuppressWarnings("deprecation")
	public void calc() {
		store = new Store();

		// Variables, they range from 1 to 5 pieces of each (domain) and has a name.

		IntVar beefPatty = new IntVar(store, "beef", 1, 5);
		IntVar bun = new IntVar(store, "bun", 1, 5);
		IntVar cheese = new IntVar(store, "cheese", 1, 5);
		IntVar onion = new IntVar(store, "onion", 1, 5);
		IntVar pickle = new IntVar(store, "pickle", 1, 5);
		IntVar lettuce = new IntVar(store, "lettuce", 1, 5);
		IntVar tomato = new IntVar(store, "ketchup", 1, 5);
		IntVar ketchup = new IntVar(store, "tomato", 1, 5);

		// Burger consists of all its ingredients, and they have an amount within their
		// domain.
		IntVar[] burger = { beefPatty, bun, cheese, onion, pickle, lettuce, ketchup, tomato };

		// setting the different ingredients nutrition information. Vector position
		// corresponds to which ingredient.
		int[] sodium = { 50, 330, 310, 1, 260, 3, 160, 3 };
		int[] fat = { 17, 9, 6, 2, 0, 0, 0, 0 };
		int[] calories = { 220, 260, 70, 10, 5, 4, 20, 9 };

		// adding constraints with maximum of any given measurement.
		store.impose(new LinearInt(store, burger, sodium, "<", 3000));
		store.impose(new LinearInt(store, burger, fat, "<", 150));
		store.impose(new LinearInt(store, burger, calories, "<", 3000));

		// adding constraint making sure equal amounts of certain ingredients.
		store.impose(new XeqY(ketchup, lettuce));
		store.impose(new XeqY(tomato, pickle));

		// a variable which we would like to mimimize. Domain could be from zero to
		// negative infinity. it doesnt really matter.
		IntVar cost = new IntVar(store, "cost", -10000000, 0);
		// prices in cent (to adjust to Ints) vector position corresponding to items.
		int[] price = { -25, -15, -10, -9, -3, -4, -2, -4 };

		// Variable cost should be set to the number of pieces of each multiplied with
		// its price.
		store.impose(new LinearInt(store, burger, price, "==", cost));

		// checking if there is a solution
		if (store.consistency()) {
			Search<IntVar> search = new DepthFirstSearch<IntVar>();
			//null is a tiebreaker. If a both solutions are equal, this determines. 
			SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(burger, null, new IndomainMin<IntVar>());
			boolean result = search.labeling(store, select, cost);

			System.out.println(result + " hej");
			if (result) {
				int totalSodium = 0;
				int totalCal = 0;
				int totalfat = 0;
				for (int i = 0; i < burger.length; i++) {
					System.out.println(burger[i]);
					totalSodium += burger[i].value() * sodium[i];
					totalfat += burger[i].value() * fat[i];
					totalCal += burger[i].value() * calories[i];

				}
				System.out.println("Cost: " + -0.01*cost.value()+ " USD");
				System.out.println("Sodium: " + totalSodium);
				System.out.println("Fat: " + totalfat);
				System.out.println("Calories: " + totalCal);

			}

		}

	}

}
