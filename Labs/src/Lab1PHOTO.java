import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.DepthFirstSearch;
import org.jacop.search.IndomainMin;
import org.jacop.search.Search;
import org.jacop.search.SelectChoicePoint;
import org.jacop.search.SimpleSelect;

public class Lab1PHOTO {
	private Store store;
	private int n;
	private int n_prefs;
	private int[][] prefs;

	public static void main(String[] args) {
		Lab1PHOTO lab1 = new Lab1PHOTO();
		lab1.setPreferences();
	//	lab1.MimimizeMaxDistance();
		lab1.MinimizeBrokenPreferences();

	}

	private void setPreferences() {
		 n = 9;
		 n_prefs = 17;
		 prefs = new int[][]{{1,3}, {1,5}, {1,8},
		   {2,5}, {2,9}, {3,4}, {3,5}, {4,1},
		   {4,5}, {5,6}, {5,1}, {6,1}, {6,9},
		   {7,3}, {7,8}, {8,9}, {8,7}};
//		
//		 n = 11;
//		 n_prefs = 20;
//		 prefs = new int[][]{{1,3}, {1,5}, {2,5},
//		   {2,8}, {2,9}, {3,4}, {3,5}, {4,1},
//		   {4,5}, {4,6}, {5,1}, {6,1}, {6,9},
//		   {7,3}, {7,5}, {8,9}, {8,7}, {8,10},
//		   {9, 11}, {10, 11}};
//
//		n = 15;
//		n_prefs = 20;
//		prefs = new int[][] { { 1, 3 }, { 1, 5 }, { 2, 5 }, { 2, 8 }, { 2, 9 }, { 3, 4 }, { 3, 5 }, { 4, 1 }, { 4, 15 },
//				{ 4, 13 }, { 5, 1 }, { 6, 10 }, { 6, 9 }, { 7, 3 }, { 7, 5 }, { 8, 9 }, { 8, 7 }, { 8, 14 }, { 9, 13 },
//				{ 10, 11 } };

		store = new Store();

	}

	private void MimimizeMaxDistance() {

		IntVar[] persons = new IntVar[n];
		Distance[] dPref = new Distance[n_prefs];
		IntVar[] distances = new IntVar[dPref.length];
		IntVar max = new IntVar(store, "max", 1, 1000);

		for (int i = 0; i < n; i++) {
			persons[i] = new IntVar(store, "Person " + (i + 1), 1, n);
		}

		for (int i = 0; i < n_prefs; i++) {
			dPref[i] = new Distance(persons[prefs[i][0] - 1], persons[prefs[i][1] - 1], new IntVar(store, 1, n - 1));
			distances[i] = dPref[i].z;
			store.impose(dPref[i]);
		}

		store.impose(new Max(distances, max));
		store.impose(new Alldifferent(persons));

		Search<IntVar> label = new DepthFirstSearch<IntVar>();
		SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(persons, null, new IndomainMin<IntVar>());

		boolean result = label.labeling(store, select, max);

		if (result) {
			for (IntVar person : persons) {
				System.out.println(person);
			}

		}

	}

	private void MinimizeBrokenPreferences() {
		IntVar[] persons = new IntVar[n];

		for (int i = 0; i < n; i++) {
			persons[i] = new IntVar(store, "Person " + (i + 1), 1, n);
		}

		Distance[] dPref = new Distance[n_prefs];

		for (int i = 0; i < n_prefs; i++) {
			dPref[i] = new Distance(persons[prefs[i][0] - 1], persons[prefs[i][1] - 1], new IntVar(store, 1, 1));
		}
		
		IntVar[] brokenPrefs = new IntVar[n_prefs];
		
		for (int i = 0; i < n_prefs; i++) {
			brokenPrefs[i] = new IntVar(store, ":" + i, 0, 1);
			store.impose(new IfThenElse(dPref[i], new XeqC(brokenPrefs[i], 0), new XeqC(brokenPrefs[i], 1)));

		}

		IntVar cost = new IntVar(store, "cost", 0, n_prefs);
		store.impose(new Alldifferent(persons));
		store.impose(new SumBool(store, brokenPrefs, "==", cost));

		Search<IntVar> label = new DepthFirstSearch<IntVar>();
		SelectChoicePoint<IntVar> select = new SimpleSelect<IntVar>(persons, null, new IndomainMin<IntVar>());

		boolean result = label.labeling(store, select, cost);

		if (result) {
			for (IntVar person : persons) {
				System.out.println(person);
			}
		}
	}

}
